/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soap;

import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import persistence.Emoticon;
import persistence.EmoticonQuery;

/**
 *
 * @author student
 */
@WebService(serviceName = "EmotesWebService")
@Stateless()
public class EmotesWebService {

    
    /**
     * Web service operation
     * @return 
     */
    @WebMethod(operationName = "getEmoticons")
    public List<Emoticon> getEmoticons() {
        return new EmoticonQuery().getAllEmoticons();
    }

    /**
     * Web service operation
     * @param id
     * @return 
     */
    @WebMethod(operationName = "getEmoticon")
    public Emoticon getEmoticon(@WebParam(name = "id") int id) {
        return new EmoticonQuery().getEmoticonById(id);
    }

    /**
     * Web service operation
     * @param id
     */
    @WebMethod(operationName = "deleteEmoticon")
    public boolean deleteEmoticon(@WebParam(name = "id") int id) {
        try{
            return new EmoticonQuery().deleteEmoticonById(id);
        } catch (Exception e){
            return false;
        }
    }

    /**
     * Web service operation
     * @param emoticon
     */
    @WebMethod(operationName = "uploadEmoticon")
    public boolean uploadEmoticon(@WebParam(name = "emoticon") Emoticon emoticon) {
        try{
            return new EmoticonQuery().insertEmoticon(emoticon);
        } catch (Exception e){
            return false;
        }
    }
    
}
