package persistence;
// Generated 2022-06-15 21:37:07 by Hibernate Tools 4.3.1


import java.util.Date;


public class Emoticon  implements java.io.Serializable {

    private int id;
    private String name;
    private String description;
    private Date created;
    private byte[] content;

    public Emoticon() {
    }

	
    public Emoticon(int id) {
        this.id = id;
    }
    public Emoticon(int id, String name, String description, Date created) {
       this.id = id;
       this.name = name;
       this.description = description;
       this.created = created;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public Date getCreated() {
        return this.created;
    }
    
    public void setCreated(Date created) {
        this.created = created;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}


