/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author student
 */
public class EmoticonQuery {
     
    public List<Emoticon> getAllEmoticons(){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction t = session.beginTransaction();
        try{
            Query query = session.createQuery("from Emoticon");
            List<Emoticon> list = query.list();
            t.commit();
            return list;
        } catch(HibernateException e){
            e.printStackTrace();
            t.rollback();
            return null;
        }
    }
    
    public Emoticon getEmoticonById(int id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction t = session.beginTransaction();
        try{
            Object obj = session.get(Emoticon.class, id);
            Emoticon emoticon = obj == null ? null : (Emoticon) obj;
            t.commit();
            return emoticon;
        } catch(HibernateException e){
            e.printStackTrace();
            t.rollback();
            return null;
        }
    }
    
    public boolean deleteEmoticonById(int id){
        Emoticon emoticon = getEmoticonById(id);
        if(emoticon == null) return false;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction t = session.beginTransaction();
        try{
          
            session.delete(emoticon);
            t.commit();
            return true;
        } catch(HibernateException e){
            e.printStackTrace();
            t.rollback();
            return false;
        }
    }
    
    
    public boolean insertEmoticon(Emoticon emoticon){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction t = session.beginTransaction();
        try{
            session.save(emoticon);
            t.commit();
            return true;
        } catch(HibernateException e){
            e.printStackTrace();
            t.rollback();
            return false;
        }
    }
}
