package beans;

import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import webservice.EmotesWebService;
import webservice.EmotesWebService_Service;
import webservice.Emoticon;
/**
 *
 * @author student
 */
@Stateless
@LocalBean
public class ControllerBean {

    
    
    
    
    public List<Emoticon> getAllEmoticons(){
        EmotesWebService_Service service = new EmotesWebService_Service();
        EmotesWebService port = service.getEmotesWebServicePort();
        List<Emoticon> list = port.getEmoticons();
        return list;
    }
    
    
    public Emoticon getEmoticon(int id){
        EmotesWebService_Service service = new EmotesWebService_Service();
        EmotesWebService port = service.getEmotesWebServicePort();
        Emoticon emoticon = port.getEmoticon(id);
        return emoticon;
    }
    
    public boolean upload(Emoticon e){
        try{
            EmotesWebService_Service service = new EmotesWebService_Service();
            EmotesWebService port = service.getEmotesWebServicePort();
            return port.uploadEmoticon(e);
        } catch (Exception ex){
            return false;
        }
    }
    
    
    public boolean delete(int id){
        try{
            EmotesWebService_Service service = new EmotesWebService_Service();
            EmotesWebService port = service.getEmotesWebServicePort();
            return port.deleteEmoticon(id);
        } catch (Exception ex){
            return false;
        }
    }
    

    public String getEmoticonDataUrl(byte[] bytes){
        if(bytes == null) return "data:image/png;base64,";
        return "data:image/png;base64," + Base64.getEncoder().encodeToString(bytes);
    }
}
