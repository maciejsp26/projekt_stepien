<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gallery</title>
    </head>
    <body style="text-align: center;">
        <div style="width: 90%; margin: auto;">
            <h1>Emoticons gallery</h1>
        <jsp:useBean id="controller" class="beans.ControllerBean" scope="session"/>
        
        <div style="text-align: center;">
            <a href="add.jsp"><button>Add new</button></a>
        </div><br/>
        
        <c:forEach var="emoticon" items="${controller.getAllEmoticons()}">
            <a style="text-decoration: none; color: black;" href="show.jsp?id=${emoticon.id}">
                <div style="border: 1px dashed lightgray;  padding: 5px; width: 100px; height: 100px; display: inline-block; text-align: center; margin-bottom: 20px;">
                    <img style="max-width: 100px; max-height: 80px; min-height: 100px;" src="${controller.getEmoticonDataUrl(emoticon.content)}"/><br/>
                    <c:out value="${emoticon.name}"/>
                </div>
            </a>
        </c:forEach>
        <br/>
    </div>    
    </body>
</html>
