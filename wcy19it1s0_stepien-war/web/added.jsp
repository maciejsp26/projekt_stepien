<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="javax.xml.datatype.DatatypeFactory"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>
<%@page import="webservice.Emoticon"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Emoticon added</title>
    </head>
    <body style="text-align: center;">
        <jsp:useBean id="controller" scope="session" class="beans.ControllerBean"/>
        <%
            String name = request.getParameter("name");
            String description = request.getParameter("desc");
            Emoticon e = new Emoticon();
            e.setName(name);
            e.setDescription(description);
            GregorianCalendar cal = new GregorianCalendar();
            XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            e.setCreated(xmlCal);
            Part part = request.getPart("image");
            byte[] bytes = new byte[(int)part.getSize()];
            part.getInputStream().read(bytes);
            e.setContent(bytes);
            boolean success = controller.upload(e);
            pageContext.setAttribute("success", success);
        %>
        <c:choose>
            <c:when test="${pageScope.success == true}">
                <h1>Emoticon added!</h1>
            </c:when>    
            <c:otherwise>
                <h1 style="color: red;">An error occured!</h1>
            </c:otherwise>
        </c:choose>
                <a href="gallery.jsp"><button>Go back</button></a>
                <a href="add.jsp"><button>Add another</button></a>
    </body>
</html>
