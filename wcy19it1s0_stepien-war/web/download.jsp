<%@page import="webservice.Emoticon"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Download emoticon</title>
    </head>
    <body>
        <jsp:useBean class="beans.ControllerBean" id="controller" scope="session"/>
        <%
            Emoticon emoticon = controller.getEmoticon(Integer.parseInt(request.getParameter("id")));
            response.setContentType("image/png");
            response.setHeader("Content-disposition", "attachment; filename="+emoticon.getName()+".png");
            response.getOutputStream().write(emoticon.getContent());
            response.getOutputStream().close();
        %>
        <jsp:forward page="show.jsp?id=${param.id}"/>
    </body>
</html>
