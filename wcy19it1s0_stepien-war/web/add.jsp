<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add emoticon</title>
    </head>
    <body style="text-align: center;">
        <h1>Uploading new emoticon</h1>
        <form action="added.jsp" method="POST" enctype="multipart/form-data" style="margin: auto; width: 500px">
            <table style="width: 100%; text-align: right;">
                <tr>
                    <td style="padding-right: 10px">Name</td>
                    <td><input type="text" name="name" style="width: 100%;"/></td>
                </tr>
                <tr>
                    <td style="padding-right: 10px">Description</td>
                    <td><textarea rows="5" name="desc" style="width: 100%;"></textarea></td>
                </tr>
                <tr>
                    <td style="padding-right: 10px">Image</td>
                    <td><input type="file" name="image" accept="image/*" style="width: 100%;"/></td>
                </tr>
                <tr><td></td><td><input type="submit" value="Save" style="width: 100%;"/></td></tr>
            </table>
        </form>
    </body>
</html>
