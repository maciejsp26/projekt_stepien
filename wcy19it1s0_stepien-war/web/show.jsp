<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Emoticon preview</title>
    </head>
    <body style="text-align: center;">
        <jsp:useBean id="controller" class="beans.ControllerBean" scope="session"/>
        <c:set var="emoticon" scope="page" value="${controller.getEmoticon(param.id)}"></c:set>
        <h1>${emoticon.name}</h1>
        <img src="${controller.getEmoticonDataUrl(emoticon.content)}" style="min-height: 500px; max-height: 500px; max-width: 500px"/>
        <br/>
        <br/>
        <div style="width: 500px; text-align: left; margin: auto;">
            Description:<br/>
            ${emoticon.description}
        </div>
        <br/>
        <a href="gallery.jsp"><button>Back to gallery</button></a>
        <a href="download.jsp?id=${emoticon.id}"><button>Download</button></a>
        <a href="delete.jsp?id=${emoticon.id}"><button>Delete</button></a>
    </body>
</html>
